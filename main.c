#include <stdio.h>
int main() {

    int choice;
    double firstNum, secondNum, answer;



    printf("\nEnter the first number: \n");
    scanf("%lf", &firstNum);

    printf("\nEnter the second number: \n");
    scanf("%lf", &secondNum);

    do{
        printf("\nMenu\n1. Add\n2. Subtract\n3. Multiply\n4.Divide\n5.Quit\nChoice:\n");
        scanf("%d", &choice);

        switch (choice){
            case 1: answer = (firstNum + secondNum);
                printf("The sum is %.0lf", answer);
                break;

            case 2:
                answer = (firstNum - secondNum);
                printf("The subtraction equals to %.0lf", answer);
                break;

            case 3: answer = (firstNum * secondNum);
                printf("The multiplication equals to %.0lf", answer);
                break;

            case 4: answer = (firstNum / secondNum);
                printf("The division equals to %.2lf", answer);
                break;

        }

    } while(choice!=5);



    return 0;
}
